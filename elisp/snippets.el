;;; Package --- Summary

;;; Commentary:

;;; Code:
(require 'personal)

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))

(use-package aas
  :hook (rust-ts-mode . aas-activate-for-major-mode)
  :hook (emacs-lisp-mode . aas-activate-for-major-mode)
  :hook (org-mode . aas-activate-for-major-mode)
  :hook (markdown-mode . aas-activate-for-major-mode)
  :functions aas-set-snippets
  :config
  (aas-set-snippets 'org-mode
    "]a" "#+AUTHOR: Aleksandr Petrosyan"
    "]t" "#+TITLE: "
    "]d" (lambda () (interactive) (insert "#+DATE: ") (insert (shell-command-to-string "echo -n $(date +%Y-%m-%d)")))
	"<aside" "#+BEGIN_aside"
	">aside" "#+END_aside"
	"<quot" "#+BEGIN_QUOTE"
	">quot" "#+END_QUOTE"
	"<src" "#+BEGIN_SRC"
	">src" "#+END_SRC"
	"]r"
	(lambda ()
	  (interactive)
	  (insert "#+BEGIN_SRC rust\n")
	  (save-excursion (insert "\n#+END_SRC"))
	  (funcall-interactively 'org-edit-special))
	"teh" "the"
	"e.g." "/e.g./ "
	"i.e." "/i.e./ "
	". " ".  "
	"? " "?  "
	"! " "!  ")
  (aas-set-snippets 'markdown-mode
	". " ".  "
	"? " "?  "
	"! " "!  "
	"e.g." "_e.g._"
	"i.e." "_i.e._")
  (aas-set-snippets 'emacs-lisp-mode
    "]up" #'insert-use-package
    "(defun" #'insert-interactive-defun
    "(define-key" #'insert-define-key
	"(keymap-set" #'insert-define-key)
  (aas-set-snippets 'rust-ts-mode
	"mod test" "#[cfg(test)]\nmod test {\n    "
	"fnnew" "/// Construct [`Self`]\n#[must_use]\npub fn new() -> Self {\ntodo!()\n}"
	"muu" "#[must_use]"
	"#d" "#[derive()]"
	"#c" "#[cfg(feature = "
	"//t" "// TODO: "
	"]t" "todo!()"
	"..in" ".into()"
	"..cl" ".clone()"
	"..asr" ".as_ref()"
	"..too" ".to_owned()"
	"]s" "self."
	"]p" "println!(\"{}\");"
	";;" #'add-semicolon-at-end)
  :ensure t)

(defun insert-interactive-defun ()
  "Insert interactive `defun`."
  (interactive)
  (insert "(defun (")
  (save-excursion
	(insert ")\n\"\"\n(interactive))")))

(defun insert-define-key (key)
  "Insert a `define-key` definition, that is persistent and binds KEY."
  (interactive "KSet key: ")
  (ensure-separate-form)
  (insert "(keymap-set (current-global-map) (kbd \"" (key-description key) "\") '")
  (company-complete))

(defun ensure-separate-form ()
  "Ensure that the form is properly spaced."
  (interactive)
  (when (my:at-indentation) (forward-paragraph) (insert "\n"))
  (when (and (eolp) (not (bolp))) (insert "\n\n")))

(defun insert-use-package ()
  "Insert the name of an uninstalled package as plain text."
  (interactive)
  (ensure-separate-form)
  (insert "(use-package ")
  (save-excursion (insert "\n:ensure t)") (indent-for-tab-command)))

(defun add-semicolon-at-end ()
  "Insert semicolon at the end of a line."
  (interactive)
  (move-end-of-line 1)
  (insert ";"))



(provide 'snippets)
;;; snippets.el ends here
