;;; personal --- my personal function definitions.  -*- lexical-binding: t; -*-

;;; Commentary:
;; personal function file

;;; Code:
(notify-send "Installing personal packages")

(define-key input-decode-map [?\C-m] (kbd "<C-m>"))

(use-package diminish
  :ensure t)

(use-package eldoc
  :ensure t
  ;; Let's just use the built-in
  :custom
  (eldoc-documentation-strategy 'eldoc-documentation-compose-eagerly)
  (eldoc-echo-area-prefer-doc-buffer t)
  (eldoc-idle-delay 0.2)
  (eldoc-minor-mode-string nil)
  :bind
  ("C-x 4 d" . eldoc-doc-buffer))

;; (defun display-ansi-colors ()
;;   (interactive)
;;   (ansi-color-apply-on-region (point-min) (point-max)))

(defun ansi-colour-no-modify()
  "Colourise the display of a buffer using `tty-format'."
  (interactive)
  (require 'tty-format)
  (format-decode-buffer 'ansi-colors))

(add-to-list 'auto-mode-alist '("\\.log\\'" . display-ansi-colors))

(defun delete-whole-line ()
  "Delete the entire line *without* putting it in the `kill-ring`."
  (interactive)
  (delete-region
   (line-beginning-position)
   (line-end-position)))

(defun delete-end-of-line ()
  "Delete (i.e. remove without adding to kill ring) from point to end of line."
  (interactive)
  (delete-region (point) (line-end-position)))

(defun my:before-whitespace ()
  "Return t if the point lies before white-space."
  (interactive "P")
  (string-match-p "^\\s-+" (buffer-substring-no-properties (point) (line-end-position))))

(defun my:after-whitespace ()
  "Return non-nil if we're after white-space."
  (interactive "P")
  (string-match-p "\\s-+$" (buffer-substring-no-properties (line-beginning-position) (point))))

(defun my:at-indentation ()
  "Return t if at indentation."
  (interactive "P")
  ;; TODO: consider making these more efficient
  (string-match-p "^\\s-+$" (buffer-substring-no-properties (line-beginning-position) (point))))

(defun my:interword ()                  ;; TODO: implement function
  "Return t if inside a word."
  (interactive "P"))

(defun switch-to-minibuffer-or-consult-mode-command ()
  "Switch to mini-buffer window."
  (interactive)
  (if (active-minibuffer-window)
      (select-window (active-minibuffer-window))
    (consult-mode-command)))

(defun increment-number-at-point ()     ;; TODO: add numerical argument
  "Increment the number at point."
  (interactive)
  (skip-chars-backward "0-9")
  (or (looking-at "[0-9]+")
      (error "No number at point"))
  (replace-match
   (number-to-string
    (1+
     (string-to-number (match-string 0))))))

(defalias 'toggle-proportional-fonts 'variable-pitch-mode)


(defun my:LaTeX-insert-frac ()
  "Insert a fraction based on the compose key."
  (interactive)
  (let ((key last-input-event))
    (message "%s" key)
    (cond
     ((equal key 189) (insert "\\frac{1}{2}"))
     ((equal key 8531) (insert "\\frac{1}{3}"))
     ((equal key 188) (insert "\\frac{1}{4}"))
     ((equal key 8533) (insert "\\frac{1}{5}"))
     ((equal key 8534) (insert "\\frac{2}{5}"))
     ((equal key 184) (insert "\\frac{3}{5}"))
     ((equal key 183) (insert "\\frac{4}{5}"))
     ((equal key 8537) (insert "\\frac{1}{6}"))
     ((equal key 181) (insert "\\frac{1}{7}"))
     ((equal key 175) (insert "\\frac{1}{8}"))
     ((equal key 8532) (insert "\\frac{2}{3}"))
     ((equal key 173) (insert "\\frac{3}{4}"))
     ((equal key 172) (insert "\\frac{5}{4}"))
     ((equal key 171) (insert "\\frac{6}{4}"))
     ((equal key 170) (insert "\\frac{5}{6}"))
     ((equal key 169) (insert "\\frac{7}{8}"))
     (t (insert "")))))

(defun is-top-line ()
  "Non-nil if at the top of the buffer."
  (interactive)
  (eq (line-beginning-position) 1))

(when (eq system-type 'darwin)
  (progn
    (customize-set-value 'mac-command-modifier 'super)
    (customize-set-value 'mac-option-modifier 'meta)
    (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
    (add-to-list 'default-frame-alist '(ns-appearance . dark))))


(defun nav-back ()
  "Navigate back by line."
  (interactive)
  (if (my:at-indentation) (beginning-of-line)
    (if (bolp) (left-char) (back-to-indentation))))

(defun nav-forward ()
  "Navigate forward by line."
  (interactive)
  (if (eolp)
	  (right-char)
    (if (and (bolp) (my:before-whitespace))
		(forward-to-indentation 0)
	  (end-of-visual-line))))

(defun kill-dwim ()
  "Kill smartly.

If at the beginning or end of line, kill the whole line.
If in region, kill the region.
If anything else, delete s-exp"
  (interactive)
  (if (use-region-p) (kill-region 0 0 t)
    (if (or (bolp) (eolp) (my:at-indentation)) (kill-whole-line) (kill-sexp))))


(defun delete-forward-char-dwim ()
  "Delete forward smartly.

Delete a single character normally.
Delete a join line if at EOL.
Delete the region (without killing)"
  (interactive)
  (if (use-region-p) (delete-active-region nil)
    (if (eolp) (join-line 1)
      (delete-char 1))))

(defun my:backward-delete-char ()
  "Fuck doc-strings."
  (if (fboundp 'ivy-delete-char)  (ivy-backward-delete-char) (delete-char -1)))

(defun delete-backward-char-dwim ()
  "Delete backward smartly.

Delete a single character normally.
Join line if at BOL.
At indentation, `delete-indentation`.
Delete region without killing."
  (interactive)
  (if (use-region-p) (delete-active-region nil)
    (if (my:at-indentation) (delete-indentation)
      (if (bolp) (join-line) (my:backward-delete-char)))))

(defun my:rust:impl ()
  "Add impl block in Rust code."
  (interactive)
  (let
      ((name-of-struct
		(treesit-node-text
         (treesit-node-child-by-field-name
          (treesit-node-inside-p
		   (treesit-node-at (point)) "struct_item") "name") t)))
    (cond
     (name-of-struct
      (treesit-end-of-defun 1)
      (newline)
      (insert "impl ")
      (insert name-of-struct))
     ((and (or (bolp) (my:at-indentation))
           (not (treesit-node-inside-p (treesit-node-at (point)) "function_item")))
      (insert "impl ") (company-complete))
     (t (self-insert-command 1)))))



(defun my:rust:previous-line-docstring-p ()
  "Non-nil if previous line is a rust //! doc-string."
  (interactive)
  (save-excursion
    (forward-line -1)
    (re-search-forward "^//!" (+ 3 (line-end-position)) t)))

(defun treesit-node-inside-p (node type)
  "Recurse up the tree of nodes to quickly check if a NODE is of TYPE."
  (cond ((null node) nil)
        ((string= type (treesit-node-type node)) node)
        (node (treesit-node-inside-p (treesit-node-parent node) type))))

(defun my:rust:doc ()
  "Add a doc-comment."
  (interactive)
  (progn
    (insert (if (or (my:rust:previous-line-docstring-p) (is-top-line)) "//! " "\n/// "))
    (indent-for-tab-command)))

(defun my:rust:fn (&optional fn-name arguments return)
  "Add fn item with the name FN-NAME(ARGUMENTS) -> RETURN."
  (interactive)
  (when (or (bolp) (my:at-indentation))
	(let ((function-name (if fn-name fn-name "function"))
		  (return-clause (if return (concat " -> " return) "")))
	  (progn
		(insert "fn ")
		(indent-for-tab-command)
		(insert (format "%s(%s)%s {\n" function-name (or arguments "") return-clause))
		(save-excursion
		  (indent-for-tab-command)
		  (insert "todo!()\n")
		  (insert "}")
		  (indent-for-tab-command))))))

(defun my:rust:test ()
  "Add test item."
  ;; TODO: check if inside `mod test'
  (interactive)
  (insert "#[test]\n")
  (funcall-interactively 'my:rust:fn "test"))

(defun my:rust:ok ()
  "Add OK at the end of the function call."
  (interactive)
  (if (treesit-node-inside-p (treesit-node-at (point)) "function_item")
	  (if (use-region-p)
		  (let ((start (region-beginning))
				(end (region-end)))
			(save-excursion
			  (goto-char end)
			  (insert ")")
			  (goto-char start)
			  (insert "Ok(")))
		(back-to-indentation)
		(unless (looking-at "Ok")
		  (insert "Ok(")
		  (when (eolp) (insert "()"))
		  (save-excursion
			(end-of-visual-line)
			(insert ")")))))
  (funcall-interactively 'my:rust:fn "fallible_function" "" "Result<(), Box<dyn std::error::Error>>"))

(defun my:org-rust-src ()
  "Add Rust source block."
  (interactive)
  (insert "#+BEGIN_SRC rust\n")
  (save-excursion (insert "\n#+END_SRC")))

(defun my:rust:pub ()
  "Cycle through visibility modifiers for Rust items."
  (interactive)
  (save-excursion
    (back-to-indentation)
    (if (looking-at "pub(crate) ")
        (progn (replace-match "") (indent-for-tab-command))
      (if (looking-at "pub")
          (progn (forward-word) (insert "(crate)"))
        (insert "pub ")))))


(defun select-and-yank-dwim ()
  "Select and yank smartly."
  (interactive)
  (if (use-region-p) (kill-ring-save 0 0 t)
    (if (my:at-indentation) (mark-paragraph)
      (expreg-expand))))

(setq make-backup-files nil)
(delete-selection-mode)
(defalias 'yes-or-no-p 'y-or-n-p)

(setq revert-without-query '(".*"))
(setq-default tab-width 4)
(tool-bar-mode -1)
(abbrev-mode 1)
(defalias 'delete-bookmark 'bookmark-delete)
(setq save-silently t)

(provide 'personal)
;;; personal.el ends here

;; Local Variables:
;; jinx-local-words: "Ok Recurse el fn frac impl todo"
;; End:
